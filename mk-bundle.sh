#!/bin/sh
# Make a bundle with all fonts, e.g. for Windows/macOS users
set -eu

dir0="$PWD"
DESTDIR="${DESTDIR:-bundle}"
VERSION="${VERSION:-2.0.0}"
# first install bundled fonts
make DESTDIR="$DESTDIR" install
# now take fonts from external packages (see dependencies in debian/control)
deps="$(grep '^Recommends: ' debian/control | tr ', ' ' ' | sed -e 's,^Recommends: ,,g')"
mkdir -p "${DESTDIR}/downloaded" "${DESTDIR}/real_bundle"
cd "${DESTDIR}/downloaded"
for i in ${deps}
do
	apt download "$i" || exit 1
done
ls -la
for i in $(ls *.deb)
do
	ar p "$i" data.tar.xz | xz -d > "${i}.tar"
	tar -xf "${i}.tar"
	rm -f data.tar.xz
done
cd "$dir0"
find "${DESTDIR}/downloaded" -iname '*.ttf' -exec cp {} "${DESTDIR}/real_bundle"  \;
find "${DESTDIR}/usr" -iname '*.ttf' -exec cp {} "${DESTDIR}/real_bundle"  \;
mv ${DESTDIR}/real_bundle/* "${DESTDIR}/"
rm -fr "${DESTDIR}/downloaded"
rm -fr "${DESTDIR}/usr"
rm -fr "${DESTDIR}/real_bundle"
mv "$DESTDIR" "dumalogiya-fonts_v${VERSION}"
zip -r "dumalogiya-fonts_v${VERSION}.zip" "dumalogiya-fonts_v${VERSION}"
rm -fr "dumalogiya-fonts_v${VERSION}"
