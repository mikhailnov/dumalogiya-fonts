# dumalogiya-fonts

`Шрифты для презентаций Думалогии.pdf` — список рекомендуемых к использованию шрифтов с описанием и образцами шрифтов.

`Шрифты для презентаций Думалогии.fodt` — исходник PDF в LibreOffice.

В [тегах](https://gitlab.com/mikhailnov/dumalogiya-fonts/tags) лежит zip-архив со сборником шрифтов.

сборник шрифтов создается командой `make bundle`.

## Установка всех шрифтов на Windows
Все файлы из zip-архива из [тегов](https://gitlab.com/mikhailnov/dumalogiya-fonts/tags) положить в `C:\Windows\Fonts`

## Установка всех шрифтов на Linux
На Ubuntu/Debian установить deb-пакет из [тегов](https://gitlab.com/mikhailnov/dumalogiya-fonts/tags).

Или все файлы из zip-архива положить в `~/.local/share/fonts/` (создать эту папку при необходимости), выпонлить `fc-cache`.
 
