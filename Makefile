VERSION = 2.0.0
PREFIX = /usr
TRUETYPE_FONTS_DIR = $(DESTDIR)/$(PREFIX)/share/fonts/truetype/dumalogiya-fonts/

all:
	@ echo "Nothing to compile. Use: make install, make uninstall"

install:
	install -d "$(TRUETYPE_FONTS_DIR)/"
	find . -iname '*.ttf' -exec cp {} "$(TRUETYPE_FONTS_DIR)/"  \;
	
uninstall:
	rm -fvr "$(TRUETYPE_FONTS_DIR)"

bundle:
	env VERSION="$(VERSION)" ./mk-bundle.sh
